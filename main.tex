\documentclass[preprint]{elsarticle}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}										
\usepackage{amssymb}
\usepackage{lineno}
\usepackage{amsmath} 
\usepackage[textwidth=80pt]{todonotes}
\usepackage[shortlabels]{enumitem} 
\usepackage[plain]{fancyref}
\graphicspath{{images/}}

\journal{Simulation Modeling Practice and Theory}

\begin{document}
	
\begin{frontmatter}

\title{On the stability of PSO when flocking}

\author[inaoe]{Rubén Bresler-Camps\corref{pcpal}}
\ead{ruben.bresler@inaoep.mx}
\author[inaoe]{Gustavo Rodríguez-Gómez\corref{auth}}
\ead{grodrig@inaoep.mx}
\author[inaoe]{Aurelio López-López\corref{auth}}
\ead{allopez@inaoep.mx}
\address[inaoe]{Instituto Nacional de Astrofísica, Óptica y Electrónica}

\cortext[auth]{Corresponding author}
\cortext[pcpal]{Principal corresponding author}

\begin{abstract}

The use of flocking algorithms has been studied to coordinate the movement in an environment with one or several targets. Other improvements such as the ability to avoid obstacles in the environment have been previously proposed to bring them closer to a potential application in robotic systems and unmanned vehicles. In this work, we perform an analysis of the stability of the PSO algorithm used in multi-target flock algorithm with restrictions in the environment. We analyze and propose the ranges of values where the parameters of the algorithm have to be defined in order to achieve convergence to the objective. We also analyze the change of speed of the agents when they are close to a collision and propose a new function to handle this situation.

\end{abstract}

\begin{keyword}
	Particle Swarm Optimization \sep Stability Analysis
\end{keyword}

\end{frontmatter}

\linenumbers

\section{Introduction}

From the perspective of the mathematical model, flocking is the collective motion of a set of self-organized individuals. The entire group coordinates to match velocity and directions and to stay together while they move. This ability allows them to keep cohesive formations.

There is also a multi-objective variation in which individuals choose a target in the environment and move towards its position, avoiding obstacles in the environment and collisions among them. This is the main focus in previous works by Barrera et al \cite{Barrera2012a}. In such work, the aim was to overcome the selection strategy and achieve flocking using a particle swarm optimization (PSO) algorithm.

In general terms, the purpose of PSO is to minimize an arbitrary objective function. In the problem space, each particle is located at a possible solution and the algorithm model defines particles movements, so they improve their position and reach iteratively a better solution.

PSO is an evolutionary algorithm where each individual within the population, known as particle, adjusts its moving trajectory in the multi-dimensional search space according to its own previous experience together with those of the neighboring particles in the swarm \cite{Kennedy1995}. 

When considering a $D$-dimensional search space, position and velocity for the $i^{th}$ particle are represented by $\vec{X}_i=x_{i1}, x_{i2}, ..., x_{iD}$ and $\vec{V}_i=v_{i1}, v_{i2}, ..., v_{iD}$, respectively. The performance of each particle is assessed through a problem-specific performance measure, which is the basis for updating $\vec{X}_i$. The best-known position of the $i^{th}$ particle, termed as personal/previous best, is represented by $\vec{P}_i=p_{i1}, p_{i2}, ..., p_{iD}$. In this way, we denote by $p_g$ the best position among all particles. Velocity and position of the $i^{th}$ particle are updated according to the following expressions,

\begin{equation} \label{eq:pso-velocity}
  \begin{split}
  	v_i(k+1) &=c_1v_i(k)  \\
  	         &+ c_2r_1(p_i(k) - x_i(k)) \\
             &+ c_3r_2(p_g(k) - x_i(k))  
  \end{split}
\end{equation}

\begin{equation} \label{eq:pso-position}
x_i(k+1) = x_i(k) + v_i(k+1)
\end{equation}

\noindent where $0<c_1<1$ and $c_2,c_3 > 0$, and factors $r_1$ and $r_2$ are uniformly distributed random values between 0 and 1. 

In this work, we focus on performing a stability analysis of the proposed algorithm in the work of \citet{Barrera2012a} to correctly determine the constraints that have to be met to converge to a stable solution. Also, we propose a new method for estimating the speed of particles when they are close to a collision in the environment.

\section{Multitarget Flocking for Constrained Environments}

In comparison with PSO basic model, the \textit{best agent position}  previously expressed in \Fref{eq:pso-velocity} was replaced by the target position $x_t$ \cite{Barrera2012a}. This change was done because of the dynamic nature of environment, and works as a navigational feedback \cite{Olfati-Saber2006}. Since we did not change the basic structure of PSO, we can analyze the stability with the conventional simplifications for PSO convergence study.

\subsection{Stability Analysis} \label{sec:stability-analysis}

We performed the stability analysis based on a simplified model as in \cite{Engelbrecht2006}. We show the necessary and sufficient conditions to ensure the stability in the system, as expressed by (\ref{eq:pso-velocity}) and (\ref{eq:pso-position}). Let assume that $x_t$ and $p_g$ in \fref{eq:pso-velocity} are constant values. The system then can be further simplified by defining $\phi = r_1 c_2+r_2c_3$ and $z_i=(r_1c_2x_t+r_2c_3p_g)/\phi$. Thus, velocity can be expressed as:

\begin{equation} \label{eq3}
v_i(k+1)=c_1v_i(k)+\phi(z_i-x_i(k))
\end{equation}

Then, setting $w_i(k)=z_i+x_i(k)$ and employing \fref{eq:pso-velocity} and (\ref{eq3}), the following linear system can be obtained:

\begin{equation} \label{eq4}
\begin{pmatrix}
v_i(k+1) \\
w_i(k+1)
\end{pmatrix} 
= 
\begin{pmatrix}
c_1  & \phi \\
-c_1 & (1-\phi)
\end{pmatrix}
\begin{pmatrix}
v_i(k) \\
w_i(k)
\end{pmatrix} 
\end{equation}

Let $M_i$ be the matrix of constant values in (\ref{eq4}). To study the stability of such linear system, the \textit{spectral radius} of $M_i$ denoted as $\rho(M_i)$, has to be analyzed, subject to $\rho(M_i) < 1$. Three conditions are necessary and sufficient to satisfy this constraint, according to \cite{Rodriguez-Gomez2004}: 

\begin{enumerate}[label=\emph{\alph*})]
\item $-[tr(M_i)+det(M_i)] < 1$
\item $det(M_i) < 1$
\item $tr(M_i)-det(M_i)<1$
\end{enumerate}

\noindent where, $tr(M_i)=c_1+(1-\phi)$ and the determinant of $M_i$ is equal to $c_1$, i.e. $det(M_i)=c_1$. We obtain the following three conditions that must satisfy the matrix $M_i$ for which $\rho(M_i)<1$ \cite{Rodriguez-Gomez2004}:

\begin{enumerate}[label=\emph{\alph*})]
\item $\phi < 2(1+c_1)$ \label{ieq1}
\item $c_1 < 1$         \label{ieq2}
\item $\phi > 0$        \label{ieq3}
\end{enumerate}

From the previous conditions \ref{ieq1} and \ref{ieq2}, one can obtained that $-1 < c_1 < 1$, since $\phi \in (0, c_2 + c_3]$ is enough to verify that $c_2 + c_3 < 2(1+c_1)$ is satisfied to fulfill condition (\ref{ieq1}). Thus, the element $c_1$ of matrix $M_i$ and the constants $c_2$ and $c_3$ must satisfy the following criteria to comply with $\rho(M_i)<1$:

\begin{enumerate}[label=\emph{\alph*})]
\item $c_2 + c_3 < 2(1+c_1)$
\item $-1 < c_1 < 1$
\item $0 < c_2 + c_3$
\end{enumerate}

The conditions obtained in the analysis satisfy the requirements mentioned in \cite{Rodriguez-Gomez2004} to assure stability. Thus, our results are consistent with previous work. Moreover, we can affirm that the analysis is simpler and trustworthy since it is  accomplished directly by analyzing matrix properties, instead of calculating and evaluating eigenvalues.

\subsection{Velocity improvement} \label{sec:velocity-adjustment}

The speed adjustment in agents helps to avoid collisions, either between agents or against obstacles in the environment \cite{Barrera2012a}. According to their results, the speed magnitude is calculated as follows. First, one has to obtain the nearest object that the agent might collide if it keeps the same direction. Let $d$ be the distance between such object and the agent and $D$ the minimum separation between them. Thus, velocity is calculated as $speed = r_{mx} * h(z)$, where $r_{mx}$ is the maximum speed of the agent and $z = \frac{d-D}{D}$. Function $h : \mathbb{R} \rightarrow (0,1]$ serves to constrain the speed and is defined as follows \cite{Barrera2012a}:

\begin{equation}\label{armando-vel}
h(z)=
\begin{cases}
    0.001,          & \text{if } z<0\\
    1,              & \text{if } z>1\\
    \frac{1}{1+e^{-10(z-0.4)}},& \text{otherwise}
\end{cases}
\end{equation}

In the work described by Barrera et al.\cite{Barrera2012a}, a logarithmic function is used to decrease the speed when $0 \leq z \leq 1$. As much as $z$ is close to 0, the agent could be in an imminent collision with the nearest object, so that the agent must slow down and avoid such collision. This function used in the agents  presents a discontinuity at 0 because the $\lim_{x\to0}{\frac{1}{1+e^{-10(z-0.4)}}}>0$, as shown in \Fref{fig:plot-velocity-armando}.

\begin{figure}[!ht] 
	\centering  
    \includegraphics[width=\textwidth]{logarithmic-velocity.png}
    \caption{Plot of velocity adjustment proposed by \cite{Barrera2012a}}    
    \label{fig:plot-velocity-armando}
\end{figure}

To overcome such limitation in the function, we propose a new function to model such drop in velocity when an agent is near an object in its environment. This new function aims to provide a better way to connect both boundaries when $z<0$ and $z>1$. One way to solve this problem is by using the \textit{Partition of Unity}, that allows to extend local constructions to the whole space. Thus, the proposed modification of velocity adjustment is defined by the following expression.

\begin{equation}\label{eq:own-vel}
h(z)=
\begin{cases}
    0.001,          & \text{if } z<0\\
    1,              & \text{if } z>0\\
    \frac{1}{\int_{0}^{1}{f(t)}}\int_{0}^{z}{f(t)},& \text{otherwise}
\end{cases}
\end{equation}

\noindent where $f(t)$ is defined as

\begin{equation}\label{eq:own-vel-f}
f(t)=
\begin{cases}
	e^{-(t-1)^{-2}} e^{-(t+1)^{-2}}, & \text{if } -1 < t < 1\\
    0, & \text{otherwise}
\end{cases}
\end{equation}

This function $f(t)$ is known to belong to class $C^\infty$, so the restriction for \textit{Partition of Unity} is accomplished. Thus we can obtain a continuous function that models the speed decrement of agents in proximity to another object in the environment. 

For comparison, if we plot the equation \ref{eq:own-vel} (see \Fref{fig:plot-velocity-comparation}), we can observe that unlike the function proposed by Barrera et al., this one  is effectively a continuous function with respect to the borders, i.e. when $z<0$ and $z>1$.  

\begin{figure}[!ht] 
	\centering  
    \includegraphics[width=\textwidth]{comparing-velocity.png}
    \caption{Comparing velocity adjustments}      
    \label{fig:plot-velocity-comparation}
\end{figure}

As a side effect of this proposal, we have that the maximum speed is sustained for a longer time, then drops rapidly at a point close to zero. In \Fref{sec:experiments}, we show the implications to the model of the proposed changes for the adjustment of the agents' speed.

\section{Experiments} \label{sec:experiments}

To validate the stability regions as well as the proposed function, we performed two experiments. One experiment to evaluate the influence of the negative stability zone on parameter $c_1$, subject to the restrictions to the parameters obtained in \Fref{sec:stability-analysis}. The second experiment was aimed at comparing our proposed change of speed in presence of a possible collision between objects. For all experiments, 30 runs of 2000 iterations were performed for each parameter group and the averages were taken as the final values.

\subsection{Stability Analysis Experiments}

As described above, the goal of this experiment was to evaluate the behavior of the swarm using the mentioned values of parameters when we use the negative particle velocity scaling factor $c_1$. 

To evaluate the model performance, we use the same metrics as in \cite{Barrera2012a}: quality, consistency in extension and consistency in polarization. Consistency in extension (\textit{cext}) measure the space occupied by the group of agents and consistency in polarization (\textit{cpol}) measure if agents move towards the same direction. Both measures are normalized in the interval [0,1] and collisions are penalized. Quality (\textit{qual}), is equal to $(cext+cpol)/2$.

\Fref{tab:parameter-selection} shows the combinations of parameters used for the tests. The parameter $c_1$ is selected so that it varies in the interval $(-1, 1)$ with a $0.1$ step. Parameter $c_2$ was set at all runs with a value of $0.2$. In case of $c_3$, two values were used, one maximum and one minimum, so that the inequality described in the analysis of \fref{sec:stability-analysis} was satisfied. One can verified that in all cases the criteria obtained in \fref{sec:stability-analysis} are met.

\begin{table}[!ht]
\centering
\resizebox{\linewidth}{!}{%
\begin{tabular}{|r||c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
\textbf{$c_1$} & -0.9 & -0.8 & -0.7 & -0.6 & -0.5 & -0.4 & -0.3 & -0.2 & -0.1 & 0 & 0.1 & 0.2 & 0.3 & 0.4 & 0.5 & 0.6 & 0.7 & 0.8 & 0.9 \\
\textbf{$c_2$} & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 & 0.2 \\
\textbf{$c_{3min}$} & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 & -0.1 \\
\textbf{$c_{3max}$} & -0.1 & 0.1 & 0.3 & 0.5 & 0.7 & 0.9 & 1.1 & 1.3 & 1.5 & 1.7 & 1.9 & 2.1 & 2.3 & 2.5 & 2.7 & 2.9 & 3.1 & 3.3 & 3.5 \\
\hline
\end{tabular}%
}
\caption{Parameter selection for experiments}
\label{tab:parameter-selection}
\end{table}

For all these tests, a variable amount of particles is used between 100 and 500 with steps of 50. For simplicity, only the results  for the quantities of particles that are multiples of 100 will be shown. In the same way, we only show results for values of $c_1$ in subset $c_1 \in \{ -0.9, -0.5, 0, 0.5, 0.9 \}$.

\section{Results} \label{sec:results}

As shown in the results of the previous section, the use of values for $c1$ within the stability zone maintains similar quality values.

From the quality results shown in \Fref{tab:quality-variation}, one can observe that the results when $ c_1 \le 0 $ do not have any statistical difference. Even though numerically they are smaller, since their respective means and standard deviations are in the same range of results when $ c_1> 0 $. This behavior is maintained when the amount of particles used in the execution of the algorithm is varied.

A similar analysis is obtained from tables \ref{tab:cext-variation} and \ref{tab:cpol-variation}. In both cases the values of these metrics decrease under a certain selection of parameters when $ c_1 \le 0 $ but their respective deviations remain within the ranges when $ c_1> 0 $. Therefore we can conclude that the stability region determined by the constraints of Section 2 is correct.

During the execution of these tests, the behavior of the swarm could be observed visually. In the case of tests with the coefficients $c_1 \le 0$ an interesting behavior was observed, such that some particles in certain iterations instead of advancing towards the objective took a step backwards in a sort of "repentance". This behavior should be studied in future works and perhaps could be used in applications where stampedes of people are simulated in emergency situations.

Regarding the second experiment, in \Fref{tab:collisions-by-armando} and \Fref{tab:collisions-by-me}, the variation of the number of collisions between the proposed algorithm in Barrera et al. and our proposal discussed in \Fref{sec:velocity-adjustment} is shown. Comparing both results, one can notice that our proposal presents a higher number of collisions, in contrast to the proposal in Barrera. Our proposed velocity adjustment, as shown visually in \Fref{fig:plot-velocity-comparation}, attempts to hold the maximum velocity for a longer time and then drops abruptly, which causes collisions to be more likely between the agents.

This kind of behavior can be undesirable under certain situations and restrictions. For example, if the environment were prepared for drone flight, where collisions between them are not acceptable, such results would be impractical. However, if one tries to simulate the behavior of living organisms, such as a stampede of people or animals to a panic triggered situation, this kind of approach would be more convenient to model the situation and interactions among agents.

\begin{table}[!htbp]
\centering
\resizebox{\linewidth}{!}{%
\begin{tabular}{ccc|ccccc}
\textbf{C1}   & \textbf{C2}  & \textbf{C3}   & \textbf{100}         & \textbf{200}         & \textbf{300}         & \textbf{400}         & \textbf{500}         \\
\hline
\textbf{-0.9} & \textbf{0.2} & \textbf{-0.1} & 0.8480 $\pm$9.68E-04 & 0.7985 $\pm$7.25E-04 & 0.7615 $\pm$6.28E-04 & 0.7304 $\pm$8.14E-04 & 0.7024 $\pm$1.28E-03 \\
\hline
\textbf{-0.5} & \textbf{0.2} & \textbf{-0.1} & 0.8477 $\pm$7.74E-04 & 0.7981 $\pm$6.69E-04 & 0.7609 $\pm$4.91E-04 & 0.7293 $\pm$8.76E-04 & 0.7016 $\pm$1.29E-03 \\
\textbf{-0.5} & \textbf{0.2} & \textbf{0.7}  & 0.8488 $\pm$1.12E-03 & 0.8001 $\pm$7.06E-04 & 0.7626 $\pm$6.73E-04 & 0.7315 $\pm$6.42E-04 & 0.7037 $\pm$8.95E-04 \\
\hline
\textbf{0}    & \textbf{0.2} & \textbf{-0.1} & 0.8474 $\pm$7.33E-04 & 0.7970 $\pm$5.68E-04 & 0.7597 $\pm$6.14E-04 & 0.7285 $\pm$9.34E-04 & 0.7005 $\pm$1.64E-03 \\
\textbf{0}    & \textbf{0.2} & \textbf{1.7}  & 0.8422 $\pm$1.16E-03 & 0.7972 $\pm$5.84E-04 & 0.7610 $\pm$4.55E-04 & 0.7302 $\pm$7.23E-04 & 0.7028 $\pm$7.03E-04 \\
\hline
\textbf{0.5}  & \textbf{0.2} & \textbf{-0.1} & 0.8484 $\pm$8.08E-04 & 0.7977 $\pm$7.67E-04 & 0.7601 $\pm$6.54E-04 & 0.7288 $\pm$9.01E-04 & 0.7009 $\pm$1.61E-03 \\
\textbf{0.5}  & \textbf{0.2} & \textbf{2.7}  & 0.8371 $\pm$1.14E-03 & 0.7953 $\pm$8.06E-04 & 0.7601 $\pm$7.72E-04 & 0.7295 $\pm$5.48E-04 & 0.7022 $\pm$6.55E-04 \\
\hline
\textbf{0.9}  & \textbf{0.2} & \textbf{-0.1} & 0.8481 $\pm$1.02E-03 & 0.7976 $\pm$8.05E-04 & 0.7599 $\pm$5.90E-04 & 0.7287 $\pm$7.95E-04 & 0.7010 $\pm$1.56E-03 \\
\textbf{0.9}  & \textbf{0.2} & \textbf{3.5}  & 0.8333 $\pm$1.18E-03 & 0.7939 $\pm$9.65E-04 & 0.7591 $\pm$6.71E-04 & 0.7290 $\pm$4.19E-04 & 0.7020 $\pm$8.39E-04\\
\hline
\end{tabular}%
}
\caption{Extension variation using stability zone for parameters $c_1, c_2, c_3$}
\label{tab:cext-variation}
\end{table}

\begin{table}[!htbp]
\centering
\resizebox{\linewidth}{!}{%
\begin{tabular}{ccc|ccccc}
\textbf{C1}   & \textbf{C2}  & \textbf{C3}   & \textbf{100}         & \textbf{200}         & \textbf{300}         & \textbf{400}         & \textbf{500}         \\
\hline
\textbf{-0.9} & \textbf{0.2} & \textbf{-0.1} & 0.9664 $\pm$3.56E-03 & 0.9483 $\pm$3.53E-03 & 0.9363 $\pm$4.06E-03 & 0.9241 $\pm$3.11E-03 & 0.9194 $\pm$3.79E-03 \\
\hline
\textbf{-0.5} & \textbf{0.2} & \textbf{-0.1} & 0.9681 $\pm$2.03E-03 & 0.9518 $\pm$3.16E-03 & 0.9418 $\pm$3.24E-03 & 0.9328 $\pm$2.54E-03 & 0.9241 $\pm$2.40E-03 \\
\textbf{-0.5} & \textbf{0.2} & \textbf{0.7}  & 0.9638 $\pm$1.78E-03 & 0.9522 $\pm$1.47E-03 & 0.9462 $\pm$1.99E-03 & 0.9367 $\pm$1.56E-03 & 0.9295 $\pm$1.88E-03 \\
\hline
\textbf{0}    & \textbf{0.2} & \textbf{-0.1} & 0.9700 $\pm$1.46E-03 & 0.9563 $\pm$1.50E-03 & 0.9472 $\pm$1.63E-03 & 0.9405 $\pm$1.99E-03 & 0.9308 $\pm$1.33E-03 \\
\textbf{0}    & \textbf{0.2} & \textbf{1.7}  & 0.9581 $\pm$2.03E-03 & 0.9494 $\pm$2.03E-03 & 0.9440 $\pm$2.58E-03 & 0.9357 $\pm$2.63E-03 & 0.9274 $\pm$2.16E-03 \\
\hline
\textbf{0.5}  & \textbf{0.2} & \textbf{-0.1} & 0.9703 $\pm$1.05E-03 & 0.9583 $\pm$1.14E-03 & 0.9508 $\pm$1.11E-03 & 0.9422 $\pm$1.06E-03 & 0.9357 $\pm$1.69E-03 \\
\textbf{0.5}  & \textbf{0.2} & \textbf{2.7}  & 0.9576 $\pm$3.00E-03 & 0.9512 $\pm$3.07E-03 & 0.9377 $\pm$2.33E-03 & 0.9364 $\pm$2.53E-03 & 0.9300 $\pm$3.60E-03 \\
\hline
\textbf{0.9}  & \textbf{0.2} & \textbf{-0.1} & 0.9709 $\pm$9.30E-04 & 0.9593 $\pm$1.00E-03 & 0.9501 $\pm$9.28E-04 & 0.9429 $\pm$1.11E-03 & 0.9357 $\pm$1.15E-03 \\
\textbf{0.9}  & \textbf{0.2} & \textbf{3.5}  & 0.9547 $\pm$2.83E-03 & 0.9450 $\pm$2.78E-03 & 0.9384 $\pm$3.10E-03 & 0.9326 $\pm$2.95E-03 & 0.9260 $\pm$2.72E-03\\
\hline
\end{tabular}%
}
\caption{Polarization variation using stability zone for parameters $c_1, c_2, c_3$}
\label{tab:cpol-variation}
\end{table}

\begin{table}[!htbp]
\centering
\resizebox{\linewidth}{!}{%
\begin{tabular}{ccc|ccccc}
\textbf{C1} & \textbf{C2} & \textbf{C3} & \textbf{100} & \textbf{200} & \textbf{300} & \textbf{400} & \textbf{500} \\
\hline
\textbf{-0.9} & \textbf{0.2} & \textbf{-0.1} & 0.8604 $\pm$7.09E-04 & 0.8301 $\pm$4.95E-04 & 0.8071 $\pm$5.08E-04 & 0.7879 $\pm$6.09E-04 & 0.7703 $\pm$8.59E-04 \\
\hline
\textbf{-0.5} & \textbf{0.2} & \textbf{-0.1} & 0.8689 $\pm$6.39E-04 & 0.8376 $\pm$4.26E-04 & 0.8141 $\pm$3.89E-04 & 0.7941 $\pm$6.64E-04 & 0.7763 $\pm$9.56E-04 \\
\textbf{-0.5} & \textbf{0.2} & \textbf{0.7} & 0.8810 $\pm$8.69E-04 & 0.8510 $\pm$5.85E-04 & 0.8273 $\pm$6.32E-04 & 0.8072 $\pm$6.17E-04 & 0.7891 $\pm$6.25E-04 \\
\hline
\textbf{0} & \textbf{0.2} & \textbf{-0.1} & 0.8802 $\pm$6.51E-04 & 0.8472 $\pm$4.63E-04 & 0.8230 $\pm$4.51E-04 & 0.8027 $\pm$7.48E-04 & 0.7842 $\pm$1.15E-03 \\
\textbf{0} & \textbf{0.2} & \textbf{1.7} & 0.8758 $\pm$8.07E-04 & 0.8479 $\pm$4.78E-04 & 0.8251 $\pm$4.70E-04 & 0.8053 $\pm$5.88E-04 & 0.7875 $\pm$5.49E-04 \\
\hline
\textbf{0.5} & \textbf{0.2} & \textbf{-0.1} & 0.8866 $\pm$6.80E-04 & 0.8531 $\pm$5.90E-04 & 0.8283 $\pm$5.09E-04 & 0.8077 $\pm$6.49E-04 & 0.7891 $\pm$1.10E-03 \\
\textbf{0.5} & \textbf{0.2} & \textbf{2.7} & 0.8707 $\pm$9.53E-04 & 0.8452 $\pm$6.81E-04 & 0.8230 $\pm$5.53E-04 & 0.8034 $\pm$5.49E-04 & 0.7859 $\pm$5.12E-04 \\
\hline
\textbf{0.9} & \textbf{0.2} & \textbf{-0.1} & 0.8874 $\pm$8.11E-04 & 0.8537 $\pm$7.06E-04 & 0.8288 $\pm$4.97E-04 & 0.8083 $\pm$6.07E-04 & 0.7898 $\pm$1.08E-03 \\
\textbf{0.9} & \textbf{0.2} & \textbf{3.5} & 0.8672 $\pm$1.00E-03 & 0.8433 $\pm$7.83E-04 & 0.8214 $\pm$5.79E-04 & 0.8023 $\pm$3.88E-04 & 0.7849 $\pm$6.86E-04 \\
\hline
\end{tabular}%
}
\caption{Quality variation using stability zone for parameters $c_1, c_2, c_3$}
\label{tab:quality-variation}
\end{table}

\begin{table}[!htbp]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{ccc|ccccc}
\hline
\textbf{C1}   & \textbf{C2}  & \textbf{C3}   & \textbf{100} & \textbf{200} & \textbf{300} & \textbf{400}  & \textbf{500}   \\
\hline
\textbf{-0.9} & \textbf{0.2} & \textbf{-0.1} & 0 $\pm$0       & 0 $\pm$0       & 2.87 $\pm$3.05 & 14.73 $\pm$7.23 & 52.6 $\pm$10.13  \\
\hline
\textbf{-0.5} & \textbf{0.2} & \textbf{-0.1} & 0 $\pm$0       & 0 $\pm$0       & 2.2 $\pm$2.37  & 15.2 $\pm$6.76  & 54.13 $\pm$14.32 \\
\textbf{-0.5} & \textbf{0.2} & \textbf{0.7}  & 0 $\pm$0       & 0 $\pm$0       & 0.93 $\pm$1.36 & 10.07 $\pm$5.81 & 31 $\pm$9.58     \\
\hline
\textbf{0}    & \textbf{0.2} & \textbf{-0.1} & 0 $\pm$0       & 0 $\pm$0       & 3.47 $\pm$4.1  & 19.13 $\pm$5.4  & 68.37 $\pm$19.05 \\
\textbf{0}    & \textbf{0.2} & \textbf{1.7}  & 0 $\pm$0       & 0 $\pm$0       & 0.47 $\pm$0.86 & 3.27 $\pm$2.26  & 14.3 $\pm$4.39   \\
\hline
\textbf{0.5}  & \textbf{0.2} & \textbf{-0.1} & 0 $\pm$0       & 0.13 $\pm$0.51 & 3.4 $\pm$2.93  & 20.53 $\pm$9.05 & 74.1 $\pm$13.1   \\
\textbf{0.5}  & \textbf{0.2} & \textbf{2.7}  & 0 $\pm$0       & 0 $\pm$0       & 0.27 $\pm$0.87 & 2.53 $\pm$1.96  & 10.47 $\pm$4.97  \\
\hline
\textbf{0.9}  & \textbf{0.2} & \textbf{-0.1} & 0 $\pm$0       & 0.2 $\pm$0.61  & 3.67 $\pm$4.04 & 19.53 $\pm$5.77 & 73.27 $\pm$18.79 \\
\textbf{0.9}  & \textbf{0.2} & \textbf{3.5}  & 0 $\pm$0       & 0 $\pm$0       & 0.47 $\pm$0.86 & 1.4 $\pm$1.67   & 8.73 $\pm$4.74  \\
\hline
\end{tabular}%
}
\caption{Variation of collisions with adjustment velocity proposed by \cite{Barrera2012a}}
\label{tab:collisions-by-armando}
\end{table}

\begin{table}[!htbp]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{ccc|ccccc}
\hline
\textbf{C1}   & \textbf{C2}  & \textbf{C3}   & \textbf{100} & \textbf{200} & \textbf{300}  & \textbf{400}  & \textbf{500}    \\
\hline
\textbf{-0.9} & \textbf{0.2} & \textbf{-0.1} & 0 +/-0       & 0.73 +/-0    & 9.63 +/-3.05  & 42.9 +/-7.23  & 138.77 +/-10.13 \\
\hline
\textbf{-0.5} & \textbf{0.2} & \textbf{-0.1} & 0 +/-0       & 0.87 +/-0    & 9.47 +/-2.37  & 51 +/-6.76    & 160.47 +/-14.32 \\
\textbf{-0.5} & \textbf{0.2} & \textbf{0.7}  & 0 +/-0       & 0.33 +/-0    & 5.73 +/-1.36  & 33.27 +/-5.81 & 105.5 +/-9.58   \\
\hline
\textbf{0}    & \textbf{0.2} & \textbf{-0.1} & 0 +/-0       & 0.47 +/-0    & 13.83 +/-4.1  & 61.1 +/-5.4   & 194.2 +/-19.05  \\
\textbf{0}    & \textbf{0.2} & \textbf{1.7}  & 0 +/-0       & 0.13 +/-0    & 4.47 +/-0.86  & 24.6 +/-2.26  & 74.83 +/-4.39   \\
\hline
\textbf{0.5}  & \textbf{0.2} & \textbf{-0.1} & 0 +/-0       & 0.53 +/-0.51 & 13.73 +/-2.93 & 73.23 +/-9.05 & 208.03 +/-13.1  \\
\textbf{0.5}  & \textbf{0.2} & \textbf{2.7}  & 0 +/-0       & 0.2 +/-0     & 1.67 +/-0.87  & 19.4 +/-1.96  & 55.67 +/-4.97   \\
\hline
\textbf{0.9}  & \textbf{0.2} & \textbf{-0.1} & 0 +/-0       & 0.6 +/-0.61  & 11.87 +/-4.04 & 63.2 +/-5.77  & 208.07 +/-18.79 \\
\textbf{0.9}  & \textbf{0.2} & \textbf{3.5}  & 0 +/-0       & 0.27 +/-0    & 2.47 +/-0.86  & 10.8 +/-1.67  & 47.2 +/-4.74   \\
\hline
\end{tabular}%
}
\caption{Collision variation using velocity adjustment proposed in \Fref{sec:velocity-adjustment}}
\label{tab:collisions-by-me}
\end{table}

\section{Conclusions} \label{sec:conclusions}

In this work, we performed an extended stability analysis of the multi-target flock algorithm proposed in \cite{Barrera2012a}. As a result, we showed the feasibility of taking negative values for $c_1$ along the constraints that must exist between the parameters $c_1$, $c_2$ and $c_3$ for the algorithm, so that it can converge to a solution. In our experiments, we also find an unexplored phenomenon in the behavior of the agents when $c_1<0$, in which particles with a "fear" effect are shown, where they go back in their trajectory toward the target. A new formula was also analyzed to reduce the speed of the agents against possible collisions. Our experiments concluded that, although our proposal maintained the continuity of the velocity function applied at this point, the fact of maintaining the velocity for a longer time and reducing it in a pronounced way, caused an increase in the collisions between the objects of the environment. Both the "fear" of the agents and to maintain the speed to the maximum, are distinctive characteristics that can be useful in simulations of catastrophes, where the stampede of individuals moves motivated by the fear causing them to collide looking away from the zone of danger. This type of analysis is open for future research.

\section*{References}
\bibliographystyle{model1-num-names}
\bibliography{mendeley}

\end{document}
